"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vue = require("vue");

var _vueRouter = require("vue-router");

var _CoachesList = _interopRequireDefault(require("./pages/coaches/CoachesList.vue"));

var _ContactCoach = _interopRequireDefault(require("./pages/requests/ContactCoach.vue"));

var _UserAuth = _interopRequireDefault(require("./pages/auth/UserAuth.vue"));

var _NotFound = _interopRequireDefault(require("./pages/NotFound.vue"));

var _index = _interopRequireDefault(require("./store/index.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var CoachDetail = (0, _vue.defineAsyncComponent)(function () {
  return Promise.resolve().then(function () {
    return _interopRequireWildcard(require('./pages/coaches/CoachDetail.vue'));
  });
});
var CoachRegistration = (0, _vue.defineAsyncComponent)(function () {
  return Promise.resolve().then(function () {
    return _interopRequireWildcard(require('./pages/coaches/CoachRegistration.vue'));
  });
});
var RequestsRecieved = (0, _vue.defineAsyncComponent)(function () {
  return Promise.resolve().then(function () {
    return _interopRequireWildcard(require('./pages/requests/RequestsRecieved.vue'));
  });
});
var router = (0, _vueRouter.createRouter)({
  history: (0, _vueRouter.createWebHistory)(),
  routes: [{
    path: '/',
    redirect: '/coaches'
  }, {
    path: '/coaches',
    component: _CoachesList["default"]
  }, {
    path: '/auth',
    name: 'Auth',
    component: _UserAuth["default"],
    meta: {
      requiresUnauth: true
    }
  }, {
    name: 'coachShow',
    path: '/coaches/:id',
    props: true,
    component: CoachDetail,
    children: [{
      name: 'contact',
      path: 'contact',
      component: _ContactCoach["default"],
      props: true
    }]
  }, {
    name: 'reg',
    path: '/register',
    component: CoachRegistration,
    meta: {
      requiresAuth: true
    },
    beforeEnter: function beforeEnter(to, from, next) {
      if (_index["default"].getters['coaches/isCoach']) {
        // console.log('stop');
        next('/coaches');
      } else {
        // console.log('go');
        next();
      }
    }
  }, {
    path: '/requests',
    component: RequestsRecieved,
    meta: {
      requiresAuth: true
    }
  }, {
    path: '/:notFound(.*)',
    component: _NotFound["default"]
  }]
});
router.beforeEach(function (to, _, next) {
  if (to.meta.requiresAuth && !_index["default"].getters.isAuth) {
    next('/auth');
  } else if (to.meta.requiresUnauth && _index["default"].getters.isAuth) {
    next('/coaches');
  } else {
    next();
  }
});
var _default = router;
exports["default"] = _default;