"use strict";

var _vue = require("vue");

var _router = _interopRequireDefault(require("./router.js"));

var _index = _interopRequireDefault(require("./store/index.js"));

var _App = _interopRequireDefault(require("./App.vue"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var app = (0, _vue.createApp)(_App["default"]);
app.use(_router["default"]);
app.use(_index["default"]);
app.mount('#app');