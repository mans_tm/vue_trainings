import { defineAsyncComponent } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';

import CoachesList from './pages/coaches/CoachesList.vue';
// import CoachDetail from './pages/coaches/CoachDetail.vue';
// import CoachRegistration from './pages/coaches/CoachRegistration.vue';
import ContactCoach from './pages/requests/ContactCoach.vue';
// import RequestsRecieved from './pages/requests/RequestsRecieved.vue';
import UserAuth from './pages/auth/UserAuth.vue';
import NotFound from './pages/NotFound.vue';

import store from './store/index.js';

const CoachDetail = defineAsyncComponent(() =>
  import('./pages/coaches/CoachDetail.vue')
);
const CoachRegistration = defineAsyncComponent(() =>
  import('./pages/coaches/CoachRegistration.vue')
);
const RequestsRecieved = defineAsyncComponent(() =>
  import('./pages/requests/RequestsRecieved.vue')
);

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/', redirect: '/coaches' },
    { path: '/coaches', component: CoachesList },
    {
      path: '/auth',
      name: 'Auth',
      component: UserAuth,
      meta: { requiresUnauth: true }
    },
    {
      name: 'coachShow',
      path: '/coaches/:id',
      props: true,
      component: CoachDetail,
      children: [
        {
          name: 'contact',
          path: 'contact',
          component: ContactCoach,
          props: true
        }
      ]
    },
    {
      name: 'reg',
      path: '/register',
      component: CoachRegistration,
      meta: { requiresAuth: true },
      beforeEnter: (to, from, next) => {
        if (store.getters['coaches/isCoach']) {
          // console.log('stop');
          next('/coaches');
        } else {
          // console.log('go');
          next();
        }
      }
    },
    {
      path: '/requests',
      component: RequestsRecieved,
      meta: { requiresAuth: true }
    },
    { path: '/:notFound(.*)', component: NotFound }
  ]
});

router.beforeEach((to, _, next) => {
  if (to.meta.requiresAuth && !store.getters.isAuth) {
    next('/auth');
  } else if (to.meta.requiresUnauth && store.getters.isAuth) {
    next('/coaches');
  } else {
    next();
  }
});

export default router;
