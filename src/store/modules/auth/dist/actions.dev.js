"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var timer;
var _default = {
  login: function login(context, payload) {
    return regeneratorRuntime.async(function login$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", context.dispatch('auth', _objectSpread({}, payload, {
              mode: 'login'
            })));

          case 1:
          case "end":
            return _context.stop();
        }
      }
    });
  },
  signup: function signup(context, payload) {
    return regeneratorRuntime.async(function signup$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            return _context2.abrupt("return", context.dispatch('auth', _objectSpread({}, payload, {
              mode: 'signup'
            })));

          case 1:
          case "end":
            return _context2.stop();
        }
      }
    });
  },
  auth: function auth(context, payload) {
    var mode, url, response, responseData, error, expiresIn, expirationDate;
    return regeneratorRuntime.async(function auth$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            mode = payload.mode;
            url = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyClj1uBSivBbIsdhG7MdcRLHKc86yNSTjY';

            if (mode === 'signup') {
              url = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyClj1uBSivBbIsdhG7MdcRLHKc86yNSTjY';
            }

            _context3.next = 5;
            return regeneratorRuntime.awrap(fetch(url, {
              method: 'POST',
              body: JSON.stringify({
                email: payload.email,
                password: payload.password,
                returnSecureToken: true
              })
            }));

          case 5:
            response = _context3.sent;
            _context3.next = 8;
            return regeneratorRuntime.awrap(response.json());

          case 8:
            responseData = _context3.sent;

            if (response.ok) {
              _context3.next = 13;
              break;
            }

            console.log(responseData);
            error = new Error(responseData.message || 'Failed to authenticate. Check your login data.');
            throw error;

          case 13:
            // const expiresIn = 5000;
            expiresIn = +responseData.expiresIn * 1000;
            expirationDate = new Date().getTime() + expiresIn;
            localStorage.setItem('token', responseData.idToken);
            localStorage.setItem('userId', responseData.localId);
            localStorage.setItem('tokenExpiration', expirationDate);
            timer = setTimeout(function () {
              context.dispatch('autoLogout');
            }, expiresIn);
            context.commit('setUser', {
              token: responseData.idToken,
              userId: responseData.localId
            });

          case 20:
          case "end":
            return _context3.stop();
        }
      }
    });
  },
  tryLogin: function tryLogin(context) {
    var token = localStorage.getItem('token');
    var userLocalId = localStorage.getItem('userId');
    var tokenExpiration = localStorage.getItem('tokenExpiration');
    var expiresIn = +tokenExpiration - new Date().getTime();

    if (expiresIn < 0) {
      return;
    }

    timer = setTimeout(function () {
      context.dispatch('autoLogout');
    }, expiresIn);

    if (token && userLocalId) {
      context.commit('setUser', {
        token: token,
        userId: userLocalId
      });
    }
  },
  logout: function logout(context) {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    localStorage.removeItem('tokenExpiration');
    var data = [];
    clearTimeout(timer);
    context.commit('setUser', {
      userId: null,
      token: null
    });
    context.commit('requests/setRequests', data);
  },
  autoLogout: function autoLogout(context) {
    context.dispatch('logout');
    context.commit('setAutoLogout');
  }
};
exports["default"] = _default;