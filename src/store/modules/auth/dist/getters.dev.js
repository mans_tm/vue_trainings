"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  userId: function userId(state) {
    return state.userId;
  },
  token: function token(state) {
    return state.token;
  },
  isAuth: function isAuth(state) {
    return !!state.token;
  },
  didAutoLogout: function didAutoLogout(state) {
    return state.didAutoLogout;
  }
};
exports["default"] = _default;