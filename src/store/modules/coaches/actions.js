export default {
  registerCoach({ commit, rootGetters }, data) {
    const coachData = {
      id: rootGetters.userId,
      firstName: data.first,
      lastName: data.last,
      descr: data.descr,
      hourlyRate: data.rate,
      areas: data.areas
    };

    const token = rootGetters.token;

    fetch(
      'https://vue-http-demo-8dca5-default-rtdb.firebaseio.com/coaches.json?auth=' +
        token,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(coachData)
      }
    )
      .then(response => {
        if (!response.ok) {
          throw new Error('Could not save data. Try later!');
        }
      })
      .catch(e => {
        console.log(e);
        this.error = 'Something went wrong! ' + e.message;
      });

    commit('registerCoach', coachData);
  },

  async loadCoaches(context, payload) {
    if (!payload.forceRefresh && !context.getters.shouldUpdate) {
      // console.log('Not yet!');
      return;
    }
    const response = await fetch(
      'https://vue-http-demo-8dca5-default-rtdb.firebaseio.com/coaches.json'
    );
    if (!response.ok) {
      console.log('An error occured!');
      const error = new Error('error occured!');
      throw error;
    } else {
      const responseData = await response.json();
      const results = [];

      for (const id in responseData) {
        results.push({
          id: responseData[id].id,
          firstName: responseData[id].firstName,
          lastName: responseData[id].lastName,
          descr: responseData[id].descr,
          areas: responseData[id].areas,
          hourlyRate: responseData[id].hourlyRate
        });
      }
      context.commit('saveCoaches', results);
      context.commit('setFetchTime');
    } // console.log(results);
  }
};
