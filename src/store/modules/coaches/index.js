import mutations from './mutations.js';
import actions from './actions.js';
import getters from './getters.js';

export default {
  namespaced: true,
  state() {
    return {
      coaches: [
        //   {
        //     id: '1',
        //     firstName: 'Alfred',
        //     lastName: 'Nobel',
        //     areas: ['frontend', 'backend'],
        //     descr: 'Lorem ipsum dolor sit amet lorem ipsum dolor sit amet.',
        //     hourlyRate: 10
        //   },
        //   {
        //     id: '2',
        //     firstName: 'Tomas',
        //     lastName: 'Edison',
        //     areas: ['frontend', 'career'],
        //     descr: 'Lorem ipsum dolor sit amet lorem ipsum dolor sit amet.',
        //     hourlyRate: 20
        //   },
        //   {
        //     id: '3',
        //     firstName: 'Graham',
        //     lastName: 'Bell',
        //     areas: ['frontend', 'backend', 'career'],
        //     descr: 'Lorem ipsum dolor sit amet lorem ipsum dolor sit amet.',
        //     hourlyRate: 50
        //   },
        //   {
        //     id: '4',
        //     firstName: 'Isaac',
        //     lastName: 'Newton',
        //     areas: ['frontend'],
        //     descr: 'Lorem ipsum dolor sit amet lorem ipsum dolor sit amet.',
        //     hourlyRate: 19
        //   },
        //   {
        //     id: '5',
        //     firstName: 'Nicola',
        //     lastName: 'Tesla',
        //     areas: ['backend', 'career'],
        //     descr: 'Lorem ipsum dolor sit amet lorem ipsum dolor sit amet.',
        //     hourlyRate: 100
        //   }
      ],
      lastFetch: null
    };
  },
  mutations,
  actions,
  getters
};
