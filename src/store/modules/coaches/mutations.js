export default {
  registerCoach(state, payload) {
    state.coaches.push(payload);
  },
  saveCoaches(state, results) {
    state.coaches = results.reverse();
    // console.log(state.coaches);
  },
  setFetchTime(state) {
    state.lastFetch = new Date().getTime();
  }
};
