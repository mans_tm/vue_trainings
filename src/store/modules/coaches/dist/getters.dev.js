"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  coaches: function coaches(state) {
    return state.coaches;
  },
  hasCoaches: function hasCoaches(state) {
    return state.coaches && state.coaches.length > 0;
  },
  isCoach: function isCoach(state, _, _2, rooGetters) {
    // console.log(JSON.stringify(coaches));
    var id = rooGetters.userId; // console.log(id);

    return state.coaches.some(function (coach) {
      return coach.id === id;
    });
  },
  shouldUpdate: function shouldUpdate(state) {
    var localFetch = state.lastFetch;

    if (!localFetch) {
      return true;
    }

    var currentTime = new Date().getTime(); // console.log((currentTime - localFetch) / 1000);

    return (currentTime - localFetch) / 1000 > 60;
  }
};
exports["default"] = _default;