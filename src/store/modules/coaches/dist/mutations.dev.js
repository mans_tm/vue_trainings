"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  registerCoach: function registerCoach(state, payload) {
    state.coaches.push(payload);
  },
  saveCoaches: function saveCoaches(state, results) {
    state.coaches = results.reverse(); // console.log(state.coaches);
  },
  setFetchTime: function setFetchTime(state) {
    state.lastFetch = new Date().getTime();
  }
};
exports["default"] = _default;