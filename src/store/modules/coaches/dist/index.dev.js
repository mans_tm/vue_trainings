"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mutations = _interopRequireDefault(require("./mutations.js"));

var _actions = _interopRequireDefault(require("./actions.js"));

var _getters = _interopRequireDefault(require("./getters.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = {
  namespaced: true,
  state: function state() {
    return {
      coaches: [//   {
        //     id: '1',
        //     firstName: 'Alfred',
        //     lastName: 'Nobel',
        //     areas: ['frontend', 'backend'],
        //     descr: 'Lorem ipsum dolor sit amet lorem ipsum dolor sit amet.',
        //     hourlyRate: 10
        //   },
        //   {
        //     id: '2',
        //     firstName: 'Tomas',
        //     lastName: 'Edison',
        //     areas: ['frontend', 'career'],
        //     descr: 'Lorem ipsum dolor sit amet lorem ipsum dolor sit amet.',
        //     hourlyRate: 20
        //   },
        //   {
        //     id: '3',
        //     firstName: 'Graham',
        //     lastName: 'Bell',
        //     areas: ['frontend', 'backend', 'career'],
        //     descr: 'Lorem ipsum dolor sit amet lorem ipsum dolor sit amet.',
        //     hourlyRate: 50
        //   },
        //   {
        //     id: '4',
        //     firstName: 'Isaac',
        //     lastName: 'Newton',
        //     areas: ['frontend'],
        //     descr: 'Lorem ipsum dolor sit amet lorem ipsum dolor sit amet.',
        //     hourlyRate: 19
        //   },
        //   {
        //     id: '5',
        //     firstName: 'Nicola',
        //     lastName: 'Tesla',
        //     areas: ['backend', 'career'],
        //     descr: 'Lorem ipsum dolor sit amet lorem ipsum dolor sit amet.',
        //     hourlyRate: 100
        //   }
      ],
      lastFetch: null
    };
  },
  mutations: _mutations["default"],
  actions: _actions["default"],
  getters: _getters["default"]
};
exports["default"] = _default;