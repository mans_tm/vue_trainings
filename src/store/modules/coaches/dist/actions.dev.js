"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  registerCoach: function registerCoach(_ref, data) {
    var _this = this;

    var commit = _ref.commit,
        rootGetters = _ref.rootGetters;
    var coachData = {
      id: rootGetters.userId,
      firstName: data.first,
      lastName: data.last,
      descr: data.descr,
      hourlyRate: data.rate,
      areas: data.areas
    };
    var token = rootGetters.token;
    fetch('https://vue-http-demo-8dca5-default-rtdb.firebaseio.com/coaches.json?auth=' + token, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(coachData)
    }).then(function (response) {
      if (!response.ok) {
        throw new Error('Could not save data. Try later!');
      }
    })["catch"](function (e) {
      console.log(e);
      _this.error = 'Something went wrong! ' + e.message;
    });
    commit('registerCoach', coachData);
  },
  loadCoaches: function loadCoaches(context, payload) {
    var response, error, responseData, results, id;
    return regeneratorRuntime.async(function loadCoaches$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (!(!payload.forceRefresh && !context.getters.shouldUpdate)) {
              _context.next = 2;
              break;
            }

            return _context.abrupt("return");

          case 2:
            _context.next = 4;
            return regeneratorRuntime.awrap(fetch('https://vue-http-demo-8dca5-default-rtdb.firebaseio.com/coaches.json'));

          case 4:
            response = _context.sent;

            if (response.ok) {
              _context.next = 11;
              break;
            }

            console.log('An error occured!');
            error = new Error('error occured!');
            throw error;

          case 11:
            _context.next = 13;
            return regeneratorRuntime.awrap(response.json());

          case 13:
            responseData = _context.sent;
            results = [];

            for (id in responseData) {
              results.push({
                id: responseData[id].id,
                firstName: responseData[id].firstName,
                lastName: responseData[id].lastName,
                descr: responseData[id].descr,
                areas: responseData[id].areas,
                hourlyRate: responseData[id].hourlyRate
              });
            }

            context.commit('saveCoaches', results);
            context.commit('setFetchTime');

          case 18:
          case "end":
            return _context.stop();
        }
      }
    });
  }
};
exports["default"] = _default;