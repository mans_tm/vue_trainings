export default {
  coaches(state) {
    return state.coaches;
  },
  hasCoaches(state) {
    return state.coaches && state.coaches.length > 0;
  },
  isCoach(state, _, _2, rooGetters) {
    // console.log(JSON.stringify(coaches));
    const id = rooGetters.userId;
    // console.log(id);
    return state.coaches.some(coach => coach.id === id);
  },
  shouldUpdate(state) {
    const localFetch = state.lastFetch;
    if (!localFetch) {
      return true;
    }
    const currentTime = new Date().getTime();
    // console.log((currentTime - localFetch) / 1000);
    return (currentTime - localFetch) / 1000 > 60;
  }
};
