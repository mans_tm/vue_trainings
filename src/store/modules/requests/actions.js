export default {
  async sendRequest(context, data) {
    const requestData = {
      userEmail: data.email,
      message: data.msg
    };

    const response = await fetch(
      `https://vue-http-demo-8dca5-default-rtdb.firebaseio.com/requests/${data.id}.json`,
      {
        method: 'POST',
        body: JSON.stringify(requestData)
      }
    );

    const responseData = await response.json();

    if (!response.ok) {
      const error = new Error(
        responseData.message || 'Failed to send request!'
      );
      throw error;
    }

    requestData.id = responseData.name;
    requestData.coachId = data.id;
    context.commit('sendRequest', requestData);
  },

  async fetchRequests(context) {
    if (!context.getters.shouldUpdate) {
      return;
    }
    const coachId = context.rootGetters.userId;
    const token = context.rootGetters.token;
    const response = await fetch(
      `https://vue-http-demo-8dca5-default-rtdb.firebaseio.com/requests/${coachId}.json?auth=` +
        token
    );
    const responseData = await response.json();

    if (!response.ok) {
      const error = new Error(
        responseData.message || 'Failed to send request!'
      );
      throw error;
    }

    const requests = [];
    for (const key in responseData) {
      const request = {
        id: key,
        coachId: coachId,
        userEmail: responseData[key].userEmail,
        message: responseData[key].message
      };
      requests.push(request);
      context.commit('setRequests', requests);
      context.commit('setFetchTime');
    }
  },

  clearRequests(context) {
    const data = null;
    context.commit('setRequests', data);
  }
};
