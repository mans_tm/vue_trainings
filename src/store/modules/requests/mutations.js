export default {
  sendRequest(state, payload) {
    state.requests.unshift(payload);
  },
  setRequests(state, payload) {
    state.requests = payload;
  },
  setFetchTime(state) {
    state.lastFetch = new Date().getTime();
  }
};
