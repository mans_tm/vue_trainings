export default {
  requests(state) {
    return state.requests;
  },
  hasRequests(state) {
    return state.requests && state.requests.length > 0;
  },
  requestsCount(state) {
    return state.requests.length;
  },
  shouldUpdate(state) {
    const localFetch = state.lastFetch;
    if (!localFetch) {
      return true;
    }
    const currentTime = new Date().getTime();
    return (currentTime - localFetch) / 1000 > 10;
  }
};
