"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  sendRequest: function sendRequest(context, data) {
    var requestData, response, responseData, error;
    return regeneratorRuntime.async(function sendRequest$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            requestData = {
              userEmail: data.email,
              message: data.msg
            };
            _context.next = 3;
            return regeneratorRuntime.awrap(fetch("https://vue-http-demo-8dca5-default-rtdb.firebaseio.com/requests/".concat(data.id, ".json"), {
              method: 'POST',
              body: JSON.stringify(requestData)
            }));

          case 3:
            response = _context.sent;
            _context.next = 6;
            return regeneratorRuntime.awrap(response.json());

          case 6:
            responseData = _context.sent;

            if (response.ok) {
              _context.next = 10;
              break;
            }

            error = new Error(responseData.message || 'Failed to send request!');
            throw error;

          case 10:
            requestData.id = responseData.name;
            requestData.coachId = data.id;
            context.commit('sendRequest', requestData);

          case 13:
          case "end":
            return _context.stop();
        }
      }
    });
  },
  fetchRequests: function fetchRequests(context) {
    var coachId, token, response, responseData, error, requests, key, request;
    return regeneratorRuntime.async(function fetchRequests$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            if (context.getters.shouldUpdate) {
              _context2.next = 2;
              break;
            }

            return _context2.abrupt("return");

          case 2:
            coachId = context.rootGetters.userId;
            token = context.rootGetters.token;
            _context2.next = 6;
            return regeneratorRuntime.awrap(fetch("https://vue-http-demo-8dca5-default-rtdb.firebaseio.com/requests/".concat(coachId, ".json?auth=") + token));

          case 6:
            response = _context2.sent;
            _context2.next = 9;
            return regeneratorRuntime.awrap(response.json());

          case 9:
            responseData = _context2.sent;

            if (response.ok) {
              _context2.next = 13;
              break;
            }

            error = new Error(responseData.message || 'Failed to send request!');
            throw error;

          case 13:
            requests = [];

            for (key in responseData) {
              request = {
                id: key,
                coachId: coachId,
                userEmail: responseData[key].userEmail,
                message: responseData[key].message
              };
              requests.push(request);
              context.commit('setRequests', requests);
              context.commit('setFetchTime');
            }

          case 15:
          case "end":
            return _context2.stop();
        }
      }
    });
  },
  clearRequests: function clearRequests(context) {
    var data = null;
    context.commit('setRequests', data);
  }
};
exports["default"] = _default;