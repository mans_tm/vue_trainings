"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  requests: function requests(state) {
    return state.requests;
  },
  hasRequests: function hasRequests(state) {
    return state.requests && state.requests.length > 0;
  },
  requestsCount: function requestsCount(state) {
    return state.requests.length;
  },
  shouldUpdate: function shouldUpdate(state) {
    var localFetch = state.lastFetch;

    if (!localFetch) {
      return true;
    }

    var currentTime = new Date().getTime();
    return (currentTime - localFetch) / 1000 > 10;
  }
};
exports["default"] = _default;