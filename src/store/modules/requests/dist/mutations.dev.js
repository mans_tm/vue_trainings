"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  sendRequest: function sendRequest(state, payload) {
    state.requests.unshift(payload);
  },
  setRequests: function setRequests(state, payload) {
    state.requests = payload;
  },
  setFetchTime: function setFetchTime(state) {
    state.lastFetch = new Date().getTime();
  }
};
exports["default"] = _default;