"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vuex = require("vuex");

var _index = _interopRequireDefault(require("./modules/coaches/index.js"));

var _index2 = _interopRequireDefault(require("./modules/requests/index.js"));

var _index3 = _interopRequireDefault(require("./modules/auth/index.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var store = (0, _vuex.createStore)({
  modules: {
    coaches: _index["default"],
    requests: _index2["default"],
    auth: _index3["default"]
  },
  state: function state() {
    return {
      onlineCoaches: [] // lastFetch: null

    };
  },
  getters: {// onlineCoaches(state) {
    //   return state.onlineCoaches.reverse();
    // },
    // shouldUpdate(state) {
    //   const localFetch = state.lastFetch;
    //   if (!localFetch) {
    //     return true;
    //   }
    //   const currentTime = new Date().getTime();
    //   // console.log((currentTime - localFetch) / 1000);
    //   return (currentTime - localFetch) / 1000 > 60;
    // }
  },
  actions: {// async loadCoaches(context, payload) {
    //   if (!payload.forceRefresh && !context.getters.shouldUpdate) {
    //     // console.log('Not yet!');
    //     return;
    //   }
    //   const response = await fetch(
    //     'https://vue-http-demo-8dca5-default-rtdb.firebaseio.com/coaches.json'
    //   );
    //   if (!response.ok) {
    //     console.log('An error occured!');
    //     const error = new Error('error occured!');
    //     throw error;
    //   } else {
    //     const responseData = await response.json();
    //     const results = [];
    //     for (const id in responseData) {
    //       results.push({
    //         id: responseData[id].id,
    //         firstName: responseData[id].firstName,
    //         lastName: responseData[id].lastName,
    //         descr: responseData[id].descr,
    //         areas: responseData[id].areas,
    //         hourlyRate: responseData[id].hourlyRate
    //       });
    //     }
    //     context.commit('saveCoaches', results);
    //     context.commit('setFetchTime');
    //   } // console.log(results);
    // }
  },
  mutations: {// saveCoaches(state, results) {
    //   state.onlineCoaches = results;
    //   // console.log(state.onlineCoaches);
    // },
    // setFetchTime(state) {
    //   state.lastFetch = new Date().getTime();
    // }
  }
});
var _default = store;
exports["default"] = _default;