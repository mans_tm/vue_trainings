import { createStore } from 'vuex';

import coachesModule from './modules/coaches/index.js';
import requestsModule from './modules/requests/index.js';
import authModule from './modules/auth/index.js';

const store = createStore({
  modules: {
    coaches: coachesModule,
    requests: requestsModule,
    auth: authModule
  },
  state() {
    return {
      onlineCoaches: []
      // lastFetch: null
    };
  },
  getters: {
    // onlineCoaches(state) {
    //   return state.onlineCoaches.reverse();
    // },
    // shouldUpdate(state) {
    //   const localFetch = state.lastFetch;
    //   if (!localFetch) {
    //     return true;
    //   }
    //   const currentTime = new Date().getTime();
    //   // console.log((currentTime - localFetch) / 1000);
    //   return (currentTime - localFetch) / 1000 > 60;
    // }
  },
  actions: {
    // async loadCoaches(context, payload) {
    //   if (!payload.forceRefresh && !context.getters.shouldUpdate) {
    //     // console.log('Not yet!');
    //     return;
    //   }
    //   const response = await fetch(
    //     'https://vue-http-demo-8dca5-default-rtdb.firebaseio.com/coaches.json'
    //   );
    //   if (!response.ok) {
    //     console.log('An error occured!');
    //     const error = new Error('error occured!');
    //     throw error;
    //   } else {
    //     const responseData = await response.json();
    //     const results = [];
    //     for (const id in responseData) {
    //       results.push({
    //         id: responseData[id].id,
    //         firstName: responseData[id].firstName,
    //         lastName: responseData[id].lastName,
    //         descr: responseData[id].descr,
    //         areas: responseData[id].areas,
    //         hourlyRate: responseData[id].hourlyRate
    //       });
    //     }
    //     context.commit('saveCoaches', results);
    //     context.commit('setFetchTime');
    //   } // console.log(results);
    // }
  },
  mutations: {
    // saveCoaches(state, results) {
    //   state.onlineCoaches = results;
    //   // console.log(state.onlineCoaches);
    // },
    // setFetchTime(state) {
    //   state.lastFetch = new Date().getTime();
    // }
  }
});

export default store;
